﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReliableUDP
{
	/// <summary>
	/// Helper class to calculate <seealso cref="Packet"/>s CRC.
	/// Polynomial: 0x1021.
	/// Initial Value: 0xFFFF.
	/// </summary>
	public static class Crc16
	{
		private const ushort polynomial = 0x1021;
		private const ushort init = 0xFFFF;
		static private readonly ushort[] table = new ushort[256];
		static private bool isTablePrimed = false;

		/// <summary>
		/// Calculates the CRC-16 value of given <seealso cref="byte"/> array.
		/// </summary>
		/// <param name="bytes"><seealso cref="byte"/> array on which the CRC-16 is calculated from.</param>
		/// <returns></returns>
		public static ushort ComputeChecksum(byte[] bytes)
		{
			if (!isTablePrimed)
			{
				PopulateTable();
				isTablePrimed = true;
			}

			ushort crc = init;
			for (int i = 0; i < bytes.Length; ++i)
			{
				byte index = (byte)(crc ^ bytes[i]);
				crc = (ushort)((crc >> 8) ^ table[index]);
			}
			return crc;
		}

		/// <summary>
		/// Initializes the lookup table.
		/// </summary>
		static private void PopulateTable()
		{
			ushort value;
			ushort temp;
			for (ushort i = 0; i < table.Length; ++i)
			{
				value = 0;
				temp = i;
				for (byte j = 0; j < 8; ++j)
				{
					if (((value ^ temp) & 0x0001) != 0)
					{
						value = (ushort)((value >> 1) ^ polynomial);
					}
					else
					{
						value >>= 1;
					}
					temp >>= 1;
				}
				table[i] = value;
			}
		}
	}
}
