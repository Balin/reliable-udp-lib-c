﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReliableUDP.Utils
{
	/// <summary>
	/// Helper class for use with <seealso cref="IObserver{T}"/> Interface.
	/// </summary>
	/// <typeparam name="T"><seealso cref="Type"/> passed to the <seealso cref="IObserver{T}"/> Interface.</typeparam>
	public  class Unsubscriber<T> : IDisposable
	{
		private List<IObserver<T>> observers = new List<IObserver<T>>();
		private IObserver<T> observer;

		public Unsubscriber(List<IObserver<T>> observers, IObserver<T> observer)
		{
			this.observers = observers;
			this.observer = observer;
		}

		public void Dispose()
		{
			if (observers != null && observers.Contains(observer))
			{
				observers.Remove(observer);
			}
		}
	}
}
