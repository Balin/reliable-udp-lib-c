﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Timers;
using System.Diagnostics;

using ReliableUDP.Utils;
using ReliableUDP.Common;

namespace ReliableUDP
{
	/// <summary>
	/// Class that describes an individual remote connection.
	/// </summary>
	public class Peer : IDisposable, IObservable<Message>, IObservable<StatusMessage>
	{
		/// <summary>
		/// Enumerates the possible states for a <seealso cref="Peer"/>.
		/// </summary>
		public enum State {
			/// <summary>
			/// <seealso cref="Peer"/> is disconnected.
			/// </summary>
			Disconnected,
			/// <summary>
			/// <seealso cref="Peer"/> is awaiting a reply to its connection request.
			/// </summary>
			Connecting,
			/// <summary>
			/// <seealso cref="Peer"/> requested a connection.
			/// </summary>
			Pending,
			/// <summary>
			/// <seealso cref="Peer"/> is ready to send and receive <seealso cref="Message"/>s.
			/// </summary>
			Ready,
			/// <summary>
			/// <seealso cref="Peer"/> isn't communicating and has timedout.
			/// </summary>
			NonResponsive
		}

		/// <summary>
		/// Protocol Key used to identify the application.
		/// </summary>
		private readonly uint protocolKey;

		/// <summary>
		/// Delegate describing the Method signature for tracking a <seealso cref="Message"/>.
		/// </summary>
		/// <param name="sequenceNumber"></param>
		public delegate void MessageSendCallback(int sequenceNumber, AcknowledgeResult result);

		/// <summary>
		/// <seealso cref="Peer"/> current <seealso cref="State"/>.
		/// </summary>
		public State CurrentState { get; internal set; }

		/// <summary>
		/// Round Trip Time. Time that takes for data to go back and forth from the remote <seealso cref="Peer"/>.
		/// </summary>
		public int RTT { get; private set; }

		/// <summary>
		/// Controls when the remote <seealso cref="Peer"/> is due to be sent the next <seealso cref="Packet"/>.
		/// </summary>
		private Timer updateTimer;

		/// <summary>
		/// Counts the time since the remote <seealso cref="Peer"/> sent the last message.
		/// </summary>
		private Timer timeoutTimer;

		/// <summary>
		/// Counts the time when trying to connect to the remote <seealso cref="Peer"/>.
		/// </summary>
		private Timer connectionTimeoutTimer;

		/// <summary>
		/// Flag that enables the queued <seealso cref="Message"/>s to be sent to the remote <seealso cref="Peer"/>.
		/// </summary>
		private bool isClearToUpdate = false;

		/// <summary>
		/// <seealso cref="IPAddress"/> and Port of the remote <seealso cref="Peer"/>.
		/// </summary>
		public IPEndPoint RemoteEndPoint { get; private set; }

		/// <summary>
		/// Counts how many <seealso cref="Message"/>s are queued to be sent to the remote <seealso cref="Peer"/> on the next <seealso cref="Packet"/>.
		/// </summary>
		public int Pending { get => messageSendQueue.Count; }

		/// <summary>
		/// Tells if the remote <seealso cref="Peer"/> is connected.
		/// </summary>
		public bool IsConnected { get; internal set; }

		/// <summary>
		/// object to control thread access to the <seealso cref="messageSendQueue"/>, <seealso cref="systemSendQueue"/> and <seealso cref="messageSendCallbacks"/> objects.
		/// </summary>
		private object sendMessageLock = new object();

		/// <summary>
		/// Next <seealso cref="Packet"/>s <seealso cref="SequenceNumber"/>.
		/// </summary>
		private ushort nextSequenceNumber = 0;

		/// <summary>
		/// Flag to indicate if the first <seealso cref="Packet"/> was received and <seealso cref="SequenceNumber"/>s tracking has started.
		/// </summary>
		private bool IsSequenceNumbersPrimed = false;

		/// <summary>
		/// Last remote <seealso cref="Peer"/> received <seealso cref="Packet"/>'s <seealso cref="SequenceNumber"/>.
		/// </summary>
		private SequenceNumber lastPacketSeqNumber = (SequenceNumber) 0;

		/// <summary>
		/// Tracks the last 32 <seealso cref="Packet"/>s <seealso cref="SequenceNumber"/> counting from <seealso cref="lastPacketSeqNumber"/> received from the remote <seealso cref="Peer"/>.
		/// </summary>
		private BitField lastReceivedPackets = new BitField(4);

		/// <summary>
		/// This <seealso cref="List{T}"/> is populated to ignore any duplicated <seealso cref="Packet"/>s received.
		/// Contains at most 32 <seealso cref="SequenceNumber"/>s as any number farther than 32 from the <seealso cref="lastPacketSeqNumber"/> is considered dropped.
		/// </summary>
		private List<SequenceNumber> lastReceivedSequenceNumbers = new List<SequenceNumber>();

		/// <summary>
		/// Keeps the reference to the methods of the observers tracking the sent <seealso cref="Message"/>s.
		/// Contains at most 32 <seealso cref="SequenceNumber"/>s as any number farther than 32 from the <seealso cref="lastPacketSeqNumber"/> is considered dropped.
		/// </summary>
		private Dictionary<int, MessageResult> messageSendCallbacks = new Dictionary<int, MessageResult>();

		/// <summary>
		/// High priority <seealso cref="Queue{T}"/> data containing protocol control data like:
		/// Connection request, Connection acception and Disconnection notification.
		/// </summary>
		internal Queue<byte[]> systemSendQueue = new Queue<byte[]>();

		/// <summary>
		/// Application data waiting to be sent on next <seealso cref="Packet"/>.
		/// </summary>
		private Queue<KeyValuePair<byte[],MessageSendCallback>> messageSendQueue = new Queue<KeyValuePair<byte[], MessageSendCallback>>();

		/// <summary>
		/// <seealso cref="List{T}"/> of observers for received <seealso cref="Message"/>s from the remote <seealso cref="Peer"/>.
		/// </summary>
		private List<IObserver<Message>> messageObservers = new List<IObserver<Message>>();

		/// <summary>
		/// <seealso cref="List{T}"/> of observers for <seealso cref="StatusMessage"/>s from the <seealso cref="Peer"/>. 
		/// </summary>
		private List<IObserver<StatusMessage>> statusObservers = new List<IObserver<StatusMessage>>();

		/// <summary>
		/// <seealso cref="SequenceNumber"/> of the <seealso cref="Packet"/> being used to calculate the RTT.
		/// </summary>
		private SequenceNumber rttSequenceNumber;

		/// <summary>
		/// Counts the time since the <seealso cref="Packet"/> being used for calculating the RTT was sent.
		/// </summary>
		private Stopwatch rttStopwatch = new Stopwatch();

		/// <summary>
		/// Used for checking which <seealso cref="Packet"/>s acknowledgement is delayed.
		/// </summary>
		private Stopwatch peerClock = new Stopwatch();

		/// <summary>
		/// Initializes a new instance of <seealso cref="Peer"/>.
		/// </summary>
		/// <param name="protocolKey">Protocol Key used by the application.</param>
		/// <param name="remoteEndPoint"><seealso cref="IPEndPoint"/> of the remote <seealso cref="Peer"/>.</param>
		/// <param name="updateInterval">Delay time between <seealso cref="Packet"/>s.</param>
		internal Peer(uint protocolKey, IPEndPoint remoteEndPoint, double updateInterval)
		{
			this.protocolKey = protocolKey;
			this.RemoteEndPoint = remoteEndPoint;
			this.CurrentState = State.Disconnected;

			this.updateTimer = new Timer(updateInterval);
			this.updateTimer.Stop();
			this.updateTimer.AutoReset = true;
			this.updateTimer.Elapsed += UpdateTimerCallback;

			this.timeoutTimer = new Timer(updateInterval * 32);
			this.timeoutTimer.Stop();
			this.timeoutTimer.AutoReset = false;
			this.timeoutTimer.Elapsed += TimeoutTimerCallback;

			this.connectionTimeoutTimer = new Timer(updateInterval * 32);
			this.connectionTimeoutTimer.Stop();
			this.connectionTimeoutTimer.AutoReset = false;
			this.connectionTimeoutTimer.Elapsed += ConnectionTimeoutTimerCallback;

			this.peerClock.Start();
		}

		/// <summary>
		/// Callback method to clear the sending of the next <seealso cref="Packet"/> to the remote <seealso cref="Peer"/>.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="eventArgs"></param>
		private void UpdateTimerCallback(Object source, ElapsedEventArgs eventArgs)
		{
			isClearToUpdate = true;
		}

		/// <summary>
		/// Callback method when the remote <seealso cref="Peer"/> times out.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="eventArgs"></param>
		private void TimeoutTimerCallback(Object source, ElapsedEventArgs eventArgs)
		{
			foreach (IObserver<StatusMessage> observer in statusObservers)
			{
				observer.OnNext(new StatusMessage(this, State.NonResponsive));
			}
		}

		/// <summary>
		/// Callback method when the remote <seealso cref="Peer"/> doesn't reply the connection request.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="eventArgs"></param>
		private void ConnectionTimeoutTimerCallback(Object source, ElapsedEventArgs eventArgs)
		{
			CurrentState = State.Disconnected;
			foreach (IObserver<StatusMessage> observer in statusObservers)
			{
				observer.OnNext(new StatusMessage(this, State.NonResponsive));
			}
		}

		/// <summary>
		/// Initiates a connection request queuing a <seealso cref="Packet"/> to be sent immediately.
		/// </summary>
		internal void Connect()
		{
			if(CurrentState == State.Disconnected)
			{
				CurrentState = State.Connecting;

				SequenceNumber sequenceNumber = (SequenceNumber)unchecked(nextSequenceNumber++);

				Packet packet = new Packet();
				packet.protocolKey = protocolKey;
				packet.sequenceNumber = sequenceNumber;
				packet.lastAckPacket = lastPacketSeqNumber;
				packet.previousAckPackets = lastReceivedPackets;
				packet.packetType = PacketType.ConnectionRequest;
				packet.data = null;

				connectionTimeoutTimer.Start();

				lock (sendMessageLock)
				{
					systemSendQueue.Enqueue(packet.GetBytes());
				}
			}
		}

		/// <summary>
		/// Accepts a connection request queuing a <seealso cref="Packet"/> to be sent immediately.
		/// </summary>
		public void Accept()
		{
			if(CurrentState == State.Pending)
			{
				CurrentState = State.Ready;

				SequenceNumber sequenceNumber = (SequenceNumber)unchecked(nextSequenceNumber++);

				Packet packet = new Packet();
				packet.protocolKey = protocolKey;
				packet.sequenceNumber = sequenceNumber;
				packet.lastAckPacket = lastPacketSeqNumber;
				packet.previousAckPackets = lastReceivedPackets;
				packet.packetType = PacketType.ConnectionAccept;
				packet.data = null;

				lock (sendMessageLock)
				{
					systemSendQueue.Enqueue(packet.GetBytes());
				}

				updateTimer.Start();
				timeoutTimer.Start();
			}
		}

		/// <summary>
		/// Notifies disconnection queuing a <seealso cref="Packet"/> to be sent immediately.
		/// </summary>
		public void Disconnect()
		{
			if(CurrentState == State.Ready)
			{
				CurrentState = State.Disconnected;

				SequenceNumber sequenceNumber = (SequenceNumber)unchecked(nextSequenceNumber++);

				Packet packet = new Packet();
				packet.protocolKey = protocolKey;
				packet.sequenceNumber = sequenceNumber;
				packet.lastAckPacket = lastPacketSeqNumber;
				packet.previousAckPackets = lastReceivedPackets;
				packet.packetType = PacketType.DisconnectNotify;
				packet.data = null;

				lock (sendMessageLock)
				{
					systemSendQueue.Enqueue(packet.GetBytes());
				}
			}
		}

		/// <summary>
		/// Observe for Messages received from this <seealso cref="Peer"/>.
		/// </summary>
		/// <param name="observer"></param>
		/// <returns></returns>
		public IDisposable Subscribe(IObserver<Message> observer)
		{
			if (!messageObservers.Contains(observer))
			{
				messageObservers.Add(observer);
			}
			return new Unsubscriber<Message>(messageObservers, observer);
		}

		/// <summary>
		/// Observe for status changes from this <seealso cref="Peer"/>.
		/// </summary>
		/// <param name="observer"></param>
		/// <returns></returns>
		public IDisposable Subscribe(IObserver<StatusMessage> observer)
		{
			if (!statusObservers.Contains(observer))
			{
				statusObservers.Add(observer);
			}
			return new Unsubscriber<StatusMessage>(statusObservers, observer);
		}

		/// <summary>
		/// Queues data to be sent to the remote <seealso cref="Peer"/>.
		/// </summary>
		/// <param name="buffer">Raw data to be sent.</param>
		/// <param name="messageSendCallback">Method to be called during the delivery tracking of this data</param>
		/// <returns></returns>
		public int Send(byte[] buffer, MessageSendCallback messageSendCallback)
		{
			if(CurrentState != State.Ready)
			{
				throw new SystemException("Trying to send data to a Peer that isn't ready");
			}

			lock (sendMessageLock)
			{
				messageSendQueue.Enqueue(new KeyValuePair<byte[], MessageSendCallback>(buffer, messageSendCallback));
			}

			return nextSequenceNumber;
		}

		/// <summary>
		/// Checks if there is a new <seealso cref="Packet"/> to send to the remote <seealso cref="Peer"/>.
		/// Also updates the tracking of messages.
		/// </summary>
		/// <returns></returns>
		internal byte[] DequeueNextMessage()
		{
			List<int> droppedMessages = new List<int>();

			foreach (KeyValuePair<int, MessageResult> pair in messageSendCallbacks)
			{
				int difference = (nextSequenceNumber - 1) - (SequenceNumber)pair.Key;
				if (difference > 32)
				{
					droppedMessages.Add(pair.Key);
				}
				else
				{
					if (peerClock.ElapsedMilliseconds - pair.Value.sendTime > RTT * 1.5 && !pair.Value.isLateNotified)
					{
						pair.Value.isLateNotified = true;
						foreach (Delegate @delegate in pair.Value.@delegate.GetInvocationList())
						{
							((MessageSendCallback)@delegate)(pair.Key, AcknowledgeResult.Late);
						}
					}
				}				
			}

			for (int i = 0; i < droppedMessages.Count; i++)
			{
				foreach (Delegate @delegate in messageSendCallbacks[droppedMessages[i]].@delegate.GetInvocationList())
				{
					((MessageSendCallback)@delegate)(droppedMessages[i], AcknowledgeResult.Dropped);
				}
				messageSendCallbacks.Remove(droppedMessages[i]);
			}

			if (!isClearToUpdate)
			{
				return null;
			}
			else
			{
				isClearToUpdate = false;
			}

			SequenceNumber sequenceNumber = (SequenceNumber)unchecked(nextSequenceNumber++);

			lock (sendMessageLock)
			{
				List<byte> sendBytes = new List<byte>();
				while(messageSendQueue.Count > 0)
				{
					KeyValuePair<byte[], MessageSendCallback> pendingMessage = messageSendQueue.Dequeue();
					byte[] pendingMessageLength = BitConverter.GetBytes((ushort)pendingMessage.Key.Length);

					if (BitConverter.IsLittleEndian)
					{
						Array.Reverse(pendingMessageLength);
					}

					sendBytes.AddRange(pendingMessageLength);
					sendBytes.AddRange(pendingMessage.Key);

					if(pendingMessage.Value != null)
					{
						if (!messageSendCallbacks.ContainsKey(sequenceNumber))
						{
							messageSendCallbacks.Add(sequenceNumber, new MessageResult());
						}

						messageSendCallbacks[sequenceNumber].@delegate = (MessageSendCallback)messageSendCallbacks[sequenceNumber].@delegate + pendingMessage.Value;
						messageSendCallbacks[sequenceNumber].sendTime = peerClock.ElapsedMilliseconds;
					}
				}

				Packet packet = new Packet();
				packet.protocolKey = protocolKey;
				packet.sequenceNumber = sequenceNumber;
				packet.lastAckPacket = lastPacketSeqNumber;
				packet.previousAckPackets = lastReceivedPackets;
				if (sendBytes.Count > 0)
				{
					packet.packetType = PacketType.ApplicationData;
					packet.data = sendBytes.ToArray();
				}
				else
				{
					packet.packetType = PacketType.Heartbeat;
				}

				if (!rttStopwatch.IsRunning)
				{
					rttSequenceNumber = sequenceNumber;
					rttStopwatch.Start();
				}

				return packet.GetBytes();
			}
			
		}

		/// <summary>
		/// Notifies of a <seealso cref="Packet"/> received from the remote <seealso cref="Peer"/>.
		/// </summary>
		/// <param name="packet"><seealso cref="Packet"/> received.</param>
		internal void NotifyReceivedPacket(Packet packet)
		{
			lock (sendMessageLock)
			{
				SequenceNumber receivedSequenceNumber = packet.sequenceNumber;
				int difference = receivedSequenceNumber - lastPacketSeqNumber;

				timeoutTimer.Stop();
				timeoutTimer.Start();

				/*if (receivedSequenceNumber > ushort.MaxValue - 5 || receivedSequenceNumber < ushort.MinValue + 5)
				{
					Console.WriteLine("<R.Seq.Num.:{0:D}|L.Seq.Num.:{1:D}|Diff.:{2:D}>", receivedSequenceNumber, lastPacketSeqNumber, difference);
					System.Threading.Thread.Yield();
				}*/

				if (difference > 32 || lastReceivedSequenceNumbers.Contains(receivedSequenceNumber))
				{
					//Console.WriteLine("<R.Seq.Num.:{0:D}|L.Seq.Num.:{1:D}|Diff.:{2:D}>", receivedSequenceNumber, lastPacketSeqNumber, difference);
					return;
				}

				if (difference > 0)
				{
					if (IsSequenceNumbersPrimed)
					{
						lastReceivedPackets.ShiftRight(difference);
						lastReceivedPackets[difference - 1] = true;

						lastPacketSeqNumber = receivedSequenceNumber;
						lastReceivedSequenceNumbers.Add(receivedSequenceNumber);

						while (lastReceivedSequenceNumbers.Count > 32)
						{
							lastReceivedSequenceNumbers.RemoveAt(0);
						}
					}
					else
					{
						IsSequenceNumbersPrimed = true;

						lastPacketSeqNumber = receivedSequenceNumber;
					}
				}
				else
				{
					//Console.WriteLine("<R.Seq.Num.:{0:D}|L.Seq.Num.:{1:D}|Diff.:{2:D}>", receivedSequenceNumber, lastPacketSeqNumber, difference);
					if (IsSequenceNumbersPrimed)
					{
						lastReceivedPackets.ShiftRight(difference);
						lastReceivedPackets[difference - 1] = true;

						lastPacketSeqNumber = receivedSequenceNumber;
					}
					else
					{
						IsSequenceNumbersPrimed = true;

						lastPacketSeqNumber = receivedSequenceNumber;
					}
				}

				if (messageSendCallbacks.ContainsKey(packet.lastAckPacket))
				{
					if(rttSequenceNumber == packet.lastAckPacket && rttStopwatch.IsRunning)
					{
						rttStopwatch.Stop();
						int rttAdjust = ((int)rttStopwatch.ElapsedMilliseconds - RTT)/10;
						RTT += rttAdjust;
						rttStopwatch.Reset();
					}
					foreach (Delegate @delegate in messageSendCallbacks[packet.lastAckPacket].@delegate.GetInvocationList())
					{
						((MessageSendCallback)@delegate)(packet.lastAckPacket, AcknowledgeResult.Acknowledged);
					}
					messageSendCallbacks.Remove(packet.lastAckPacket);
				}
				for (int i = 0; i < 32; i++)
				{
					SequenceNumber acknowledgeCallbackSeqNumber = packet.lastAckPacket - (SequenceNumber)(i + 1);
					if (rttSequenceNumber == acknowledgeCallbackSeqNumber && rttStopwatch.IsRunning)
					{
						rttStopwatch.Stop();
						int rttAdjust = ((int)rttStopwatch.ElapsedMilliseconds - RTT) / 10;
						RTT += rttAdjust;
						rttStopwatch.Reset();
					}

					if (packet.previousAckPackets[i])
					{
						if (messageSendCallbacks.ContainsKey(acknowledgeCallbackSeqNumber))
						{
							foreach (Delegate @delegate in messageSendCallbacks[acknowledgeCallbackSeqNumber].@delegate.GetInvocationList())
							{
								((MessageSendCallback)@delegate)(acknowledgeCallbackSeqNumber, AcknowledgeResult.Acknowledged);
							}
							messageSendCallbacks.Remove(acknowledgeCallbackSeqNumber);
						}
					}
				}

				switch (CurrentState)
				{
					case Peer.State.Connecting:
						{
							if (packet.packetType == PacketType.ConnectionAccept)
							{
								//Console.WriteLine("Peer is Ready");
								CurrentState = Peer.State.Ready;

								updateTimer.Start();
								timeoutTimer.Start();
								connectionTimeoutTimer.Stop();

								foreach (IObserver<StatusMessage> observer in statusObservers)
								{
									observer.OnNext(new StatusMessage(this, State.Ready));
								}
							}
						}
						break;
					case Peer.State.Ready:
						{
							if (packet.packetType == PacketType.ApplicationData)
							{
								List<Message> messages = new List<Message>();
								int remainingBytes = packet.data.Length;

								while (remainingBytes > 0)
								{
									byte[] messageLengthBytes = new byte[2];
									Array.Copy(packet.data, packet.data.Length - remainingBytes, messageLengthBytes, 0, 2);
									if (BitConverter.IsLittleEndian)
									{
										Array.Reverse(messageLengthBytes);
									}

									ushort messageLength = BitConverter.ToUInt16(messageLengthBytes, 0);
									remainingBytes -= 2;

									byte[] messageData = new byte[messageLength];
									Array.Copy(packet.data, packet.data.Length - remainingBytes, messageData, 0, messageLength);

									remainingBytes -= messageLength;

									messages.Add(new Message(messageData, this));
								}

								for (int i = 0; i < messages.Count; i++)
								{
									foreach (IObserver<Message> observer in messageObservers)
									{
										observer.OnNext(messages[i]);
									}
								}
							}
							else
							{
								switch (packet.packetType)
								{
									case PacketType.DisconnectNotify:
										{
											foreach (IObserver<StatusMessage> observer in statusObservers)
											{
												observer.OnNext(new StatusMessage(this, State.Disconnected));

												Disconnect();
											}
										}
										break;
									default: break;
								}
							}
						}
						break;
						/*default:
							{
								switch (packet.packetType)
								{
									case PacketType.DisconnectNotify:
										{
											foreach (IObserver<StatusMessage> observer in statusObservers)
											{
												observer.OnNext(new StatusMessage(this, PacketType.DisconnectNotify));

												Disconnect();
											}
										}
										break;
									default: break;
								}
							}
							break;*/
				}
			}
		}

		/// <summary>
		/// Disconnects the <seealso cref="Peer"/> and stops the timers.
		/// </summary>
		public void Dispose()
		{
			Disconnect();

			updateTimer.Stop();
			updateTimer.Dispose();

			timeoutTimer.Stop();
			timeoutTimer.Dispose();

			peerClock.Stop();
		}
	}
}
