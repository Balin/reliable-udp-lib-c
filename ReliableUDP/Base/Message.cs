﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace ReliableUDP
{
	/// <summary>
	/// Representation of a message received from a Peer.
	/// </summary>
	public struct Message
	{
		/// <summary>
		/// Raw data buffer.
		/// </summary>
		public byte[] buffer;

		/// <summary>
		/// Remote IP end point.
		/// </summary>
		public Peer peer;

		/// <summary>
		/// Initializes an empty instance of Message.
		/// </summary>
		/// <param name="bufferLength">Length of the data buffer in bytes.</param>
		/// <param name="remoteEndPoint">Remote IP end point.</param>
		public Message(int bufferLength, Peer peer)
		{
			buffer = new byte[bufferLength];
			this.peer = peer;
		}

		/// <summary>
		/// Initializes an instance of Message filled with <paramref name="buffer"/> bytes.
		/// </summary>
		/// <param name="buffer">Buffer to be copied to this instance.</param>
		/// <param name="remoteEndPoint">Remote IP end point.</param>
		public Message(byte[] buffer, Peer peer)
		{
			this.buffer = new byte[buffer.Length];
			buffer.CopyTo(this.buffer, 0);
			this.peer = peer;
		}
	}
}
