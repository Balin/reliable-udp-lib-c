﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace ReliableUDP
{
	/// <summary>
	/// Network level packet.
	/// </summary>
	public struct Packet
	{
		/// <summary>
		/// Key to validate packet.
		/// </summary>
		public uint protocolKey;

		/// <summary>
		/// Sequence number for this packet to keep track of dropped packets.
		/// </summary>
		public SequenceNumber sequenceNumber;

		/// <summary>
		/// Last received packet sequence number.
		/// </summary>
		public SequenceNumber lastAckPacket;

		/// <summary>
		/// Bitfield indicating which packets were received.
		/// </summary>
		public BitField previousAckPackets;

		/// <summary>
		/// Type indicating the packet purpose.
		/// </summary>
		public PacketType packetType;

		/// <summary>
		/// Raw application data.
		/// </summary>
		public byte[] data;

		/// <summary>
		/// Initializes a new instance of Packet.
		/// </summary>
		/// <param name="protocolKey">Application's Protocol Key.</param>
		/// <param name="sequenceNumber">This Packet's sequence number.</param>
		/// <param name="lastAckPacket">Last received Packet's sequence number.</param>
		/// <param name="previousAckPackets">Bitfield describing the 32 sequence numbers before the last received status.</param>
		/// <param name="packetType">This Packet's type.</param>
		/// <param name="data">Raw data transported by this Packet</param>
		public Packet(uint protocolKey, SequenceNumber sequenceNumber, SequenceNumber lastAckPacket, BitField previousAckPackets, PacketType packetType, byte[] data)
		{
			this.protocolKey = protocolKey;
			this.sequenceNumber = sequenceNumber;
			this.lastAckPacket = lastAckPacket;
			this.previousAckPackets = previousAckPackets;
			this.packetType = packetType;
			this.data = data;
		}

		/// <summary>
		/// Initializes a new instance of Packet from raw byte.
		/// Data is expected to be in the host machine endianess already.
		/// </summary>
		/// <param name="protocolKey">Application's Protocol Key.</param>
		/// <param name="sequenceNumber">This Packet's sequence number.</param>
		/// <param name="lastAckPacket">Last received Packet's sequence number.</param>
		/// <param name="previousAckPackets">Bitfield describing the 32 sequence numbers before the last received status.</param>
		/// <param name="packetType">This Packet's type.</param>
		/// <param name="data">Raw data transported by this Packet</param>
		private Packet(byte[] protocolKey, byte[] sequenceNumber, byte[] lastAckPacket, byte[] previousAckPackets, byte packetType, byte[] data)
		{
			this.protocolKey = BitConverter.ToUInt32(protocolKey, 0);
			this.sequenceNumber = (SequenceNumber)BitConverter.ToUInt16(sequenceNumber, 0);
			this.lastAckPacket = (SequenceNumber)BitConverter.ToUInt16(lastAckPacket, 0);
			this.previousAckPackets = BitField.ToBitField(previousAckPackets);
			this.packetType = (PacketType)packetType;
			this.data = data;
		}

		/// <summary>
		/// Gets the byte array format for the packet.
		/// Auto adjusts for little endian systems converting to network byte order.
		/// </summary>
		/// <returns></returns>
		public byte[] GetBytes()
		{
			bool isLittleEndian = BitConverter.IsLittleEndian;

			if(data == null)
			{
				data = new byte[0];
			}

			byte[] byteArray = new byte[13 + data.Length];

			if (!isLittleEndian)
			{
				BitConverter.GetBytes(protocolKey).CopyTo(byteArray, 0);
				BitConverter.GetBytes(sequenceNumber).CopyTo(byteArray, 4);
				BitConverter.GetBytes(lastAckPacket).CopyTo(byteArray, 6);
				previousAckPackets.GetBytes().CopyTo(byteArray, 8);
				byteArray[12] = (byte)packetType;
				data.CopyTo(byteArray, 13);
			}
			else
			{
				byte[] protocolKeyArray = BitConverter.GetBytes(protocolKey);
				Array.Reverse(protocolKeyArray);
				protocolKeyArray.CopyTo(byteArray, 0);

				byte[] sequenceNumberArray = BitConverter.GetBytes(sequenceNumber);
				Array.Reverse(sequenceNumberArray);
				sequenceNumberArray.CopyTo(byteArray, 4);

				byte[] lastAckArray = BitConverter.GetBytes(lastAckPacket);
				Array.Reverse(lastAckArray);
				lastAckArray.CopyTo(byteArray, 6);

				previousAckPackets.GetBytes().CopyTo(byteArray, 8);
				byteArray[12] = (byte)packetType;
				data.CopyTo(byteArray, 13);
			}

			return byteArray;
		}

		/// <summary>
		/// Initializes a new instance of Packet desserializing from a raw byte array.
		/// </summary>
		/// <param name="buffer">Raw byte array to be desserialized.</param>
		/// <returns>New instance of Packet</returns>
		public static Packet ToPacket(byte[] buffer)
		{
			if (buffer.Length < 13)
			{
				throw new SystemException("buffer is too small to contain a valid packet");
			}

			byte[] protocolKey = new byte[4];
			byte[] sequenceNumber = new byte[2];
			byte[] lastAckPacket = new byte[2];
			byte[] previousAckPackets = new byte[4];
			byte packetType = new byte();
			byte[] data = new byte[buffer.Length - 13];

			Array.Copy(buffer, 0, protocolKey, 0, 4);
			Array.Copy(buffer, 4, sequenceNumber, 0, 2);
			Array.Copy(buffer, 6, lastAckPacket, 0, 2);
			Array.Copy(buffer, 8, previousAckPackets, 0, 4);
			packetType = buffer[12];
			Array.Copy(buffer, 13, data, 0, buffer.Length - 13);

			if (BitConverter.IsLittleEndian)
			{
				Array.Reverse(protocolKey);
				Array.Reverse(sequenceNumber);
				Array.Reverse(lastAckPacket);
			}

			Packet packet = new Packet(protocolKey, sequenceNumber, lastAckPacket, previousAckPackets, packetType, data);

			return packet;
		}
	}
}