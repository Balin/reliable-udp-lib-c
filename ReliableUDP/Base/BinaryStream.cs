﻿using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// Base tools meant to use internally
/// </summary>
namespace ReliableUDP.Base
{
	/// <summary>
	/// Binary Representation of a Packet.
	/// Used for assisting assemblying data received by the UDP socket stream as the RUDP protocol isn't a stream oriented one.
	/// </summary>
	internal class BinaryStream
	{
		/// <summary>
		/// Packet's CRC value.
		/// </summary>
		public ushort Crc { get; set; }

		/// <summary>
		/// Packet's data total length.
		/// This value isn't checked by the CRC.
		/// </summary>
		public ushort Length { get; set; }

		/// <summary>
		/// Packet's raw byte data.
		/// This is deserialized later by the Packet class.
		/// </summary>
		public List<byte> Data { get; set; }

		/// <summary>
		/// Initializes a new instance of the BinaryStream with Data empty.
		/// </summary>
		/// <param name="crc">Packet's CRC Value.</param>
		/// <param name="length">Packet's data total length.</param>
		public BinaryStream(ushort crc, ushort length)
		{
			Crc = crc;
			Length = length;
			Data = new List<byte>();
		}
	}
}
