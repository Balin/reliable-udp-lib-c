﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReliableUDP
{
	/// <summary>
	/// Status message of a <seealso cref="Peer"/>.
	/// </summary>
	public struct StatusMessage
	{
		/// <summary>
		/// <seealso cref="Peer"/> which this status update message refers to.
		/// </summary>
		public Peer peer;

		/// <summary>
		/// <seealso cref="Peer"/> status.
		/// </summary>
		public Peer.State state;

		/// <summary>
		/// Initializes a new instance of <seealso cref="StatusMessage"/>.
		/// </summary>
		/// <param name="peer"><seealso cref="Peer"/> refered.</param>
		/// <param name="state">Status reported by this instance.</param>
		public StatusMessage(Peer peer, Peer.State state)
		{
			this.peer = peer;
			this.state = state;
		}
	}
}
