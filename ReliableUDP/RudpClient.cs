﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using ReliableUDP.Base;
using ReliableUDP.Utils;

namespace ReliableUDP
{
	/// <summary>
	/// Class that controls the flow of data for the Reliable User Datagram Protocol.
	/// </summary>
	public class RudpClient : IDisposable, IObservable<Peer>
	{
		/// <summary>
		/// Version of this library.
		/// </summary>
		public const string Version = "1.0.0";
		
		private static readonly int RECEIVE_BUFFER_LENGTH = ushort.MaxValue;
		private readonly uint protocolKey;

		private object socketLock = new object();
		private Socket socket;
		private bool isBound = false;
		private Thread receiveThread;

		private object peerLock = new object();
		private Dictionary<IPEndPoint, Peer> peerDictionary = new Dictionary<IPEndPoint, Peer>();

		private Queue<Peer> pendingPeers = new Queue<Peer>();
		private List<IObserver<Peer>> observers = new List<IObserver<Peer>>();

		public uint ProtocolKey { get => protocolKey; }
		public int Port { get; private set; }
		public double UpdateInterval { get; private set; }

		#region Socket Properties
		public Socket Client { get => socket; set => socket = value; }
		public bool DontFragment { get => socket.DontFragment; set => socket.DontFragment = value; }
		public LingerOption LingerState { get => socket.LingerState; set => socket.LingerState = value; }
		public short Ttl { get => socket.Ttl; set => socket.Ttl = value; }
		#endregion

		#region Constructors
		public RudpClient(uint protocolKey, double updateInterval)
		{
			socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
			socket.EnableBroadcast = false;
			socket.MulticastLoopback = false;

			this.protocolKey = protocolKey;

			this.UpdateInterval = updateInterval;
		}

		public RudpClient(uint protocolKey, double updateInterval, AddressFamily addressFamily)
		{
			socket = new Socket(addressFamily, SocketType.Dgram, ProtocolType.Udp);
			socket.EnableBroadcast = false;
			socket.MulticastLoopback = false;

			this.protocolKey = protocolKey;

			this.UpdateInterval = updateInterval;
		}
		#endregion

		#region Connect Methods
		public Peer Connect(IPAddress address, int port)
		{
			return Connect(new IPEndPoint(address, port));
		}

		public Peer Connect(string hostname, int port)
		{
			IPHostEntry hostEntry;

			hostEntry = Dns.GetHostEntry(hostname);

			if (hostEntry.AddressList.Length > 0)
			{
				return Connect(new IPEndPoint(hostEntry.AddressList[0], port));
			}
			else
			{
				throw new System.ArgumentException("Given hostname cannot resolve to an address");
			}
		}

		public Peer Connect(IPEndPoint endPoint)
		{
			Bind(new IPEndPoint(IPAddress.Any, endPoint.Port));

			Peer peer = new Peer(protocolKey, endPoint, UpdateInterval);
			lock (peerLock)
			{
				peerDictionary.Add(endPoint, peer);
				peer.Connect();
			}

			return peer;
		}
		#endregion
		
		public void Bind(IPEndPoint endPoint)
		{
			if (!isBound)
			{
				Port = endPoint.Port;
				socket.Bind(endPoint);

				receiveThread = new Thread(new ThreadStart(SocketUpdateMethod));
				receiveThread.Start();

				isBound = true;
			}
		}

		private void SocketUpdateMethod()
		{
			List<Peer> disconnectPeers = new List<Peer>();
			Dictionary<IPEndPoint, BinaryStream> receivedStreams = new Dictionary<IPEndPoint, BinaryStream>();
			List<IPEndPoint> clearedData = new List<IPEndPoint>();
			byte[] receiveBuffer = new byte[RECEIVE_BUFFER_LENGTH];

			while (true)
			{
				lock (socketLock)
				{
					if (socket.Poll(10, SelectMode.SelectRead) && socket.Available > 0)
					{
						
						EndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);

						int receivedLength = socket.ReceiveFrom(receiveBuffer, receiveBuffer.Length, SocketFlags.None, ref remoteEndPoint);

						if (!receivedStreams.ContainsKey((IPEndPoint)remoteEndPoint))
						{
							ushort packetCrc;
							byte[] packetCrcBytes = new byte[2];

							Array.Copy(receiveBuffer, 0, packetCrcBytes, 0, 2);
							if (BitConverter.IsLittleEndian)
							{
								Array.Reverse(packetCrcBytes);
							}
							packetCrc = BitConverter.ToUInt16(packetCrcBytes, 0);

							ushort packetLength;
							byte[] packetLengthBytes = new byte[2];

							Array.Copy(receiveBuffer, 2, packetLengthBytes, 0, 2);
							if (BitConverter.IsLittleEndian)
							{
								Array.Reverse(packetLengthBytes);
							}
							packetLength = BitConverter.ToUInt16(packetLengthBytes, 0);

							BinaryStream binaryStream = new BinaryStream(packetCrc, packetLength);

							byte[] dataBuffer = new byte[receivedLength - 4];
							Array.Copy(receiveBuffer, 4, dataBuffer, 0, dataBuffer.Length);

							binaryStream.Data.AddRange(dataBuffer);

							receivedStreams.Add((IPEndPoint)remoteEndPoint, binaryStream);
						}
						else
						{
							byte[] newData = new byte[receivedLength];
							Array.Copy(receiveBuffer, 0, newData, 0, receivedLength);
							receivedStreams[(IPEndPoint)remoteEndPoint].Data.AddRange(newData);
						}
					}

					foreach(KeyValuePair<IPEndPoint, BinaryStream> pair in receivedStreams)
					{
						IPEndPoint remoteEndPoint = pair.Key;
						BinaryStream stream = pair.Value;

						if(stream.Length >= stream.Data.Count)
						{
							byte[] originalData = stream.Data.ToArray();
							byte[] dataBuffer = new byte[stream.Length];
							Array.Copy(originalData, 0, dataBuffer, 0, stream.Length);

							ushort calculatedCrc = Crc16.ComputeChecksum(dataBuffer);

							if (stream.Length > stream.Data.Count)
							{
								byte[] leftoverData = new byte[stream.Data.Count - stream.Length];
								Array.Copy(originalData, stream.Length, leftoverData, 0, leftoverData.Length);

								ushort packetCrc;
								byte[] packetCrcBytes = new byte[2];

								Array.Copy(leftoverData, 0, packetCrcBytes, 0, 2);
								if (BitConverter.IsLittleEndian)
								{
									Array.Reverse(packetCrcBytes);
								}
								packetCrc = BitConverter.ToUInt16(packetCrcBytes, 0);

								ushort packetLength;
								byte[] packetLengthBytes = new byte[2];

								Array.Copy(leftoverData, 2, packetLengthBytes, 0, 2);
								if (BitConverter.IsLittleEndian)
								{
									Array.Reverse(packetLengthBytes);
								}
								packetLength = BitConverter.ToUInt16(packetLengthBytes, 0);

								BinaryStream binaryStream = new BinaryStream(packetCrc, packetLength);

								byte[] newDataBuffer = new byte[leftoverData.Length - 4];
								Array.Copy(leftoverData, 4, newDataBuffer, 0, newDataBuffer.Length);

								binaryStream.Data.AddRange(dataBuffer);

								receivedStreams.Add((IPEndPoint)remoteEndPoint, binaryStream);
							}

							if (stream.Crc == calculatedCrc)
							{
								Packet packet = Packet.ToPacket(dataBuffer);

								if (packet.protocolKey == protocolKey)
								{
									if (peerDictionary.ContainsKey((IPEndPoint)remoteEndPoint))
									{
										Peer peer = peerDictionary[(IPEndPoint)remoteEndPoint];

										peerDictionary[(IPEndPoint)remoteEndPoint].NotifyReceivedPacket(packet);
									}
									else
									{
										if (packet.packetType == PacketType.ConnectionRequest)
										{
											Peer peer = new Peer(protocolKey, (IPEndPoint)remoteEndPoint, UpdateInterval);
											peer.CurrentState = Peer.State.Pending;

											pendingPeers.Enqueue(peer);
										}
									}
								}
							}

							clearedData.Add(remoteEndPoint);
						}
					}

					foreach(IPEndPoint endPoint in clearedData)
					{
						receivedStreams.Remove(endPoint);
					}
				}
				lock (peerLock)
				{
					foreach(KeyValuePair<IPEndPoint, Peer> pair in peerDictionary)
					{
						IPEndPoint remoteEndPoint = pair.Key;
						Peer peer = pair.Value;

						if (peer.systemSendQueue.Count > 0)
						{
							byte[] sendBuffer = peer.systemSendQueue.Dequeue();
							List<byte> sendByteList = new List<byte>();

							ushort packetCrc = Crc16.ComputeChecksum(sendBuffer);
							byte[] packetCrcBytes = BitConverter.GetBytes(packetCrc);
							if (BitConverter.IsLittleEndian)
							{
								Array.Reverse(packetCrcBytes);
							}

							ushort packetLength = (ushort)sendBuffer.Length;
							byte[] packetLengthBytes = BitConverter.GetBytes(packetLength);
							if (BitConverter.IsLittleEndian)
							{
								Array.Reverse(packetLengthBytes);
							}

							sendByteList.AddRange(packetCrcBytes);
							sendByteList.AddRange(packetLengthBytes);
							sendByteList.AddRange(sendBuffer);

							socket.SendTo(sendByteList.ToArray(), peer.RemoteEndPoint);
						}

						if (peer.CurrentState == Peer.State.Ready)
						{
							byte[] sendBuffer = peer.DequeueNextMessage();
							if (sendBuffer != null)
							{
								List<byte> sendByteList = new List<byte>();

								ushort packetCrc = Crc16.ComputeChecksum(sendBuffer);
								byte[] packetCrcBytes = BitConverter.GetBytes(packetCrc);
								if (BitConverter.IsLittleEndian)
								{
									Array.Reverse(packetCrcBytes);
								}

								ushort packetLength = (ushort)sendBuffer.Length;
								byte[] packetLengthBytes = BitConverter.GetBytes(packetLength);
								if (BitConverter.IsLittleEndian)
								{
									Array.Reverse(packetLengthBytes);
								}

								sendByteList.AddRange(packetCrcBytes);
								sendByteList.AddRange(packetLengthBytes);
								sendByteList.AddRange(sendBuffer);

								socket.SendTo(sendByteList.ToArray(), peer.RemoteEndPoint);
							}
						}

						if(peer.CurrentState == Peer.State.Disconnected)
						{
							disconnectPeers.Add(peer);
						}
					}

					foreach(Peer peer in disconnectPeers)
					{
						peerDictionary.Remove(peer.RemoteEndPoint);
						peer.Dispose();
					}

					disconnectPeers.Clear();
				}

				while(pendingPeers.Count > 0)
				{
					Peer peer = pendingPeers.Dequeue();
					foreach(IObserver<Peer> observer in observers)
					{
						observer.OnNext(peer);
						if(peer.CurrentState == Peer.State.Ready)
						{
							peerDictionary.Add(peer.RemoteEndPoint, peer);
						}
					}
				}

				Thread.Yield();
			}
		}

		public IDisposable Subscribe(IObserver<Peer> observer)
		{
			if (!observers.Contains(observer))
			{
				observers.Add(observer);
			}
			return new Unsubscriber<Peer>(observers, observer);
		}

		public void Dispose()
		{
			lock (socketLock)
			{
				lock (peerLock)
				{
					foreach (KeyValuePair<IPEndPoint, Peer> pair in peerDictionary)
					{
						IPEndPoint remoteEndPoint = pair.Key;
						Peer peer = pair.Value;

						peer.Disconnect();

						if (peer.systemSendQueue.Count > 0)
						{
							socket.SendTo(peer.systemSendQueue.Dequeue(), peer.RemoteEndPoint);
						}
						peer.Dispose();
					}
				}

				receiveThread.Abort();
				socket.Dispose();
			}
			lock (peerLock)
			{
				peerDictionary.Clear();
			}
		}
	}
}
