﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReliableUDP
{
	/// <summary>
	/// Enumerates the possible results of a sent <seealso cref="Message"/>.
	/// </summary>
	public enum AcknowledgeResult
	{
		/// <summary>
		/// <seealso cref="Message"/> was received by the <seealso cref="Peer"/>.
		/// </summary>
		Acknowledged,
		/// <summary>
		/// <seealso cref="Message"/> hasn't been acknowledged by the<seealso cref="Peer"/> in under two Round Trip Times.
		/// </summary>
		Late,
		/// <summary>
		/// <seealso cref="Message"/> wasn't acknowledged by the <seealso cref="Peer"/> and is considered dropped.
		/// </summary>
		Dropped
	}
}
