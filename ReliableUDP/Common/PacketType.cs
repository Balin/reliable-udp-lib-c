﻿using System;
using System.Collections.Generic;

namespace ReliableUDP
{
/// <summary>
	/// Speciifes the purpose of the network packet.
	/// </summary>
	public enum PacketType
	{
		/// <summary>
		/// Virtual connection request.
		/// </summary>
		ConnectionRequest,
		/// <summary>
		/// Virtual connection acceptance.
		/// </summary>
		ConnectionAccept,
		/// <summary>
		/// Virtual connection close notification.
		/// </summary>
		DisconnectNotify,
		/// <summary>
		/// Virtual connection heartbeat containing no user data.
		/// </summary>
		Heartbeat,
		/// <summary>
		/// TODO
		/// </summary>
		EncryptionHandshake,
		/// <summary>
		/// <seealso cref="Message"/> containing <seealso cref="Packet"/>.
		/// </summary>
		ApplicationData
	}
}