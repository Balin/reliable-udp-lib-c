﻿using System;
using System.Collections.Generic;

namespace ReliableUDP
{
	/// <summary>
	/// Abstraction class for bitfields.
	/// </summary>
	public struct BitField
	{
		private bool[] bitField;

		/// <summary>
		/// Initializes an empty instance of Bitfield.
		/// </summary>
		/// <param name="byteSize">Size in bytes of the bitfield</param>
		public BitField(int byteSize)
		{
			bitField = new bool[byteSize * 8];
		}

		/// <summary>
		/// Gets of sets a bit inside the bitfield.
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public bool this[int index]
		{
			get
			{
				return bitField[index];
			}
			set
			{
				bitField[index] = value;
			}
		}

		/// <summary>
		/// Shifts the bit field right by <paramref name="amount"/>.
		/// </summary>
		/// <param name="amount"></param>
		public void ShiftRight(int amount)
		{
			for (int j = 0; j < amount; j++)
			{
				for (int i = bitField.Length - 1; i > 0; i--)
				{
					bitField[i] = bitField[i - 1];
				}
				bitField[0] = false;
			}
		}

		/// <summary>
		/// Shifts the bit field left by <paramref name="amount"/>.
		/// </summary>
		/// <param name="amount"></param>
		public void ShiftLeft(int amount)
		{
			for (int j = 0; j < amount; j++)
			{
				for (int i = 0; i < bitField.Length - 1; i++)
				{
					bitField[i] = bitField[i + 1];
				}
				bitField[bitField.Length - 1] = false;
			}
		}

		/// <summary>
		/// Gets the raw byte array representation of this bitfield.
		/// </summary>
		/// <returns></returns>
		public byte[] GetBytes()
		{
			byte[] returnArray = new byte[(int)Math.Ceiling((double)bitField.Length / 8)];

			for (int i = 0; i < returnArray.Length; i++)
			{
				for (int j = 0; j < 8; j++)
				{
					byte value = (byte)(bitField[i * 8 + j] ? 1 : 0);
					returnArray[i] |= (byte)(value << (7 - j));
				}
			}

			return returnArray;
		}

		public static BitField ToBitField(byte[] buffer)
		{
			BitField bitField = new BitField(buffer.Length);

			for (int i = 0; i < buffer.Length; i++)
			{
				for (int j = 0; j < 8; j++)
				{
					if ((buffer[i] & 1 << (7 - j)) > 0)
					{
						bitField[i * 8 + j] = true;
					}
					else
					{
						bitField[i * 8 + j] = false;
					}
				}
			}

			return bitField;
		}
	}
}