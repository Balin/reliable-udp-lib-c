﻿using System;
using System.Collections.Generic;

namespace ReliableUDP
{
	/// <summary>
	/// Represents the 16-bit numbers used for identifying each <seealso cref="Packet"/>.
	/// </summary>
	public struct SequenceNumber
	{
		private ushort value;

		private SequenceNumber(ushort value)
		{
			this.value = value;
		}

		public static explicit operator SequenceNumber(ushort value)
		{
			return new SequenceNumber(value);
		}

		public static implicit operator ushort(SequenceNumber me)
		{
			return me.value;
		}

		public static bool operator >(SequenceNumber sqOne, SequenceNumber sqTwo)
		{
			return SequenceGreaterThan(sqOne, sqTwo);
		}

		public static bool operator <(SequenceNumber sqOne, SequenceNumber sqTwo)
		{//TODO!
			return SequenceGreaterThan(sqOne, sqTwo);
		}

		private static bool SequenceGreaterThan(ushort s1, ushort s2)
		{
			return ((s1 > s2) && (s1 - s2 <= ushort.MaxValue)) ||
				   ((s1 < s2) && (s2 - s1 > ushort.MaxValue));
		}

		public static SequenceNumber operator -(SequenceNumber sqOne, SequenceNumber sqTwo)
		{
			int valueOne = sqOne;
			int valueTwo = sqTwo;

			if (valueTwo > valueOne && SequenceGreaterThan(sqOne, sqTwo))
			{
				valueOne += ushort.MaxValue;
			}
			int result = valueOne - valueTwo;

			return (SequenceNumber)result;
		}

		public override string ToString()
		{
			return value.ToString();
		}
	}
}