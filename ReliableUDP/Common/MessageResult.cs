﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReliableUDP.Common
{
	/// <summary>
	/// Groups <seealso cref="Message"/> delivery tracking related data.
	/// </summary>
	internal class MessageResult
	{
		/// <summary>
		/// Time in milisseconds at which the message was sent to <seealso cref="Peer"/>.
		/// </summary>
		public long sendTime;

		/// <summary>
		/// Tracks if the observers were already notified if tracked message is delayed.
		/// </summary>
		public bool isLateNotified;

		/// <summary>
		/// Method to be called of the observers for the tracked <seealso cref="Message"/>.
		/// </summary>
		public Delegate @delegate;
	}
}
