﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;

using ReliableUDP;

namespace ConsoleApp1
{
	class LibTest
	{
		private static RudpClient rudpClient = new RudpClient(0x00000080U, 500);

		private static int remotePort = 1337;

		private static byte[] remoteIpAddress = new byte[4] { 192, 168, 15, 16 };
		private static IPEndPoint remoteEndpoint = new IPEndPoint(new IPAddress(remoteIpAddress), remotePort);

		private static byte[] remoteIpAddressTwo = new byte[4] { 192, 168, 15, 10 };
		private static IPEndPoint remoteEndpointTwo = new IPEndPoint(new IPAddress(remoteIpAddressTwo), remotePort);

		static void Main(string[] args)
		{
			MessageSubscriber messageSubscriber = new MessageSubscriber();

			Peer remotePeer = rudpClient.Connect(remoteEndpoint);
			remotePeer.Subscribe(messageSubscriber);

			Peer remotePeerTwo = rudpClient.Connect(remoteEndpointTwo);
			remotePeerTwo.Subscribe(messageSubscriber);

			ConsoleKeyInfo lastKeyPress;
			StringBuilder stringBuilder = new StringBuilder();

			Console.WriteLine(BitConverter.IsLittleEndian.ToString());

			Packet testPacket = new Packet();

			testPacket.protocolKey = 0x01;
			testPacket.sequenceNumber = (SequenceNumber) 0x02;
			testPacket.lastAckPacket = (SequenceNumber) 0x03;
			testPacket.previousAckPackets = new BitField(4);
			testPacket.previousAckPackets[0] = true;
			testPacket.previousAckPackets[2] = true;
			testPacket.previousAckPackets[4] = true;
			testPacket.previousAckPackets[6] = true;
			testPacket.previousAckPackets[8] = true;
			testPacket.previousAckPackets[10] = true;
			testPacket.previousAckPackets[12] = true;
			testPacket.previousAckPackets[14] = true;
			testPacket.previousAckPackets[16] = true;
			testPacket.previousAckPackets[18] = true;
			testPacket.previousAckPackets[20] = true;
			testPacket.previousAckPackets[22] = true;
			testPacket.previousAckPackets[24] = true;
			testPacket.previousAckPackets[26] = true;
			testPacket.previousAckPackets[28] = true;
			testPacket.previousAckPackets[30] = true;

			testPacket.packetType = PacketType.EncryptionHandshake;
			testPacket.data = new byte[] { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06 };

			byte[]testArray = testPacket.GetBytes();

			//remotePeer.Send(testArray, null);

			for(int i = 0; i<testArray.Length; i++)
			{
				Console.Write("{0:X2}|", testArray[i]);
			}
			Console.Write("\n\r");

			Packet newPacket = Packet.ToPacket(testArray);

			Console.WriteLine("Rebuilt Packet");
			Console.WriteLine(newPacket.protocolKey);
			Console.WriteLine(newPacket.sequenceNumber);
			Console.WriteLine(newPacket.lastAckPacket);
			for(int i=0; i<32; i++)
			{
				Console.WriteLine(newPacket.previousAckPackets[i]);
			}
			Console.WriteLine(newPacket.packetType);
			for(int i=0; i<newPacket.data.Length; i++)
			{
				Console.Write("{0:X2}|", newPacket.data[i]);
			}

			while (true)
			{
				lastKeyPress = Console.ReadKey();

				if(lastKeyPress.Key == ConsoleKey.Escape)
				{
					break;
				}

				ConsoleKey currentKey = lastKeyPress.Key;

				if(currentKey == ConsoleKey.Enter || currentKey == ConsoleKey.LeftArrow)
				{
					stringBuilder.Append("\r\n");
					if(currentKey == ConsoleKey.Enter)
					{
						remotePeer.Send(ASCIIEncoding.ASCII.GetBytes(stringBuilder.ToString()), null);
					}
					else
					{
						remotePeerTwo.Send(ASCIIEncoding.ASCII.GetBytes(stringBuilder.ToString()), null);
					}
					
					stringBuilder.Clear();
				}
				else
				{
					stringBuilder.Append(lastKeyPress.KeyChar);
				}

				Thread.Yield();
			}

			rudpClient.Dispose();
		}

		class MessageSubscriber : IObserver<Message>
		{
			public void OnCompleted()
			{
				throw new NotImplementedException();
			}

			public void OnError(Exception error)
			{
				throw new NotImplementedException();
			}

			public void OnNext(Message value)
			{
				string receivedString = ASCIIEncoding.ASCII.GetString(value.buffer);
				Console.WriteLine(value.peer.RemoteEndPoint.ToString() +": "+ receivedString);
			}
		}
	}
}
