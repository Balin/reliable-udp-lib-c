﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;
using System.Net;
using System.Net.Sockets;

using ReliableUDP;

namespace MessengerApp
{
	class Messenger
	{
		private static RudpClient rudpClient = new RudpClient(0x00000080U, 20);

		private static Peer remotePeer;
		private static bool isRunning = true;

		static void Main(string[] args)
		{
			bool isAddressValid = false;

			byte[] remoteIpBytes = new byte[4];
			int remotePort = 0;

			while (!isAddressValid)
			{
				bool retry = false;

				Console.WriteLine("Input remote IP address (XXX.XXX.XXX.XXX:XXXXX): ");
				string remoteAddressString = Console.ReadLine();
				string[] addressParams = remoteAddressString.Split(new char[] { '.', ':' });

				if(addressParams.Length < 5)
				{
					Console.WriteLine("Address format is invalid, try again following the following format (XXX.XXX.XXX.XXX:XXXXX)");
					Console.WriteLine("Press Any Key to try again or ESC to exit");

					ConsoleKeyInfo pressedKey = Console.ReadKey();
					if (pressedKey.Key == ConsoleKey.Escape)
					{
						Environment.Exit(0);
					}
					else
					{
						Console.Clear();
						continue;
					}
				}

				for(int i = 0; i < 4; i++)
				{
					try
					{
						remoteIpBytes[i] = Convert.ToByte(addressParams[i]);
					}
					catch(FormatException exception)
					{
						Console.WriteLine("Address format is invalid, try again following the following format (XXX.XXX.XXX.XXX:XXXXX)");
						Console.WriteLine("Press Any Key to try again or ESC to exit");

						ConsoleKeyInfo pressedKey = Console.ReadKey();
						if(pressedKey.Key == ConsoleKey.Escape)
						{
							Environment.Exit(0);
						}
						else
						{
							retry = true;
							break;
						}
					}
				}
				if (!retry)
				{
					try
					{
						remotePort = Convert.ToInt32(addressParams[4]);
					}
					catch(FormatException exception)
					{
						Console.WriteLine("Address format is invalid, try again following the following format (XXX.XXX.XXX.XXX:XXXXX)");
						Console.WriteLine("Press Any Key to try again or ESC to exit");

						ConsoleKeyInfo pressedKey = Console.ReadKey();
						if (pressedKey.Key == ConsoleKey.Escape)
						{
							Environment.Exit(0);
						}
						else
						{
							retry = true;
							break;
						}
					}
				}
				else
				{
					Console.Clear();
					continue;
				}

				isAddressValid = true;
			}

			MessageSubscriber messageSubscriber = new MessageSubscriber();

			Peer peer = rudpClient.Connect(new IPEndPoint(new IPAddress(remoteIpBytes), remotePort));
			peer.Subscribe(messageSubscriber);
			peer.Subscribe(new StatusObserver());
			remotePeer = peer;

			ConsoleKeyInfo lastKeyPress;
			StringBuilder stringBuilder = new StringBuilder();

			Console.WriteLine("Connecting to remote: " + peer.RemoteEndPoint.ToString());
			while(peer.CurrentState != Peer.State.Ready && isRunning)
			{
				Thread.Yield();
			}

			while (isRunning)
			{
				lastKeyPress = Console.ReadKey();

				if (isRunning)
				{
					if (lastKeyPress.Key == ConsoleKey.Escape)
					{
						isRunning = false;
						break;
					}

					ConsoleKey currentKey = lastKeyPress.Key;

					if (currentKey == ConsoleKey.Enter)
					{
						stringBuilder.Append("\r\n");
						peer.Send(ASCIIEncoding.ASCII.GetBytes(stringBuilder.ToString()), messageSubscriber.MessageResultCallback);

						stringBuilder.Clear();
					}
					else
					{
						stringBuilder.Append(lastKeyPress.KeyChar);
					}
				}

				Thread.Yield();
			}

			peer.Disconnect();

			rudpClient.Dispose();

			Console.WriteLine("Disconnected! Press any key to close the program");

			Console.ReadKey();
		}

		class MessageSubscriber : IObserver<Message>
		{
			public void OnCompleted()
			{
				throw new NotImplementedException();
			}

			public void OnError(Exception error)
			{
				throw new NotImplementedException();
			}

			public void OnNext(Message value)
			{
				string receivedString = ASCIIEncoding.ASCII.GetString(value.buffer);
				Console.WriteLine(value.peer.RemoteEndPoint.ToString() + ": " + receivedString);
			}

			public void MessageResultCallback(int sequenceNumber, AcknowledgeResult result)
			{
				switch (result)
				{
					case AcknowledgeResult.Acknowledged:
						{
							Console.WriteLine("Message " + sequenceNumber + " Acknowledged | RTT: " + remotePeer.RTT);
						}
						break;
					case AcknowledgeResult.Late:
						{
							Console.WriteLine("Message " + sequenceNumber + " is Late");
						}
						break;
					case AcknowledgeResult.Dropped:
						{
							Console.WriteLine("Message " + sequenceNumber + " was Dropped");
						}
						break;
				}
			}
		}

		class StatusObserver : IObserver<StatusMessage>
		{
			public void OnCompleted()
			{
				throw new NotImplementedException();
			}

			public void OnError(Exception error)
			{
				throw new NotImplementedException();
			}

			public void OnNext(StatusMessage value)
			{
				switch (value.state)
				{
					case Peer.State.Ready:
						{
							Console.WriteLine("Connected!");
						}
						break;
					case Peer.State.Disconnected:
						{
							Console.WriteLine("Peer " + value.peer.RemoteEndPoint.ToString() + " has disconnected!, ending program!");

							isRunning = false;
						}
						break;
					case Peer.State.NonResponsive:
						{
							Console.WriteLine("Peer " + value.peer.RemoteEndPoint.ToString() + " is non responsive, ending program!");

							isRunning = false;
						}
						break;
				}
			}
		}
	}
}
