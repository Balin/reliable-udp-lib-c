﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ReliableUDP;

namespace EchoConsole
{
	class EchoConsole
	{
		private static RudpClient rudpClient = new RudpClient(0x00000080U, 20);
		private static int socketPort = 1337;
		public static List<Peer> connectedPeers = new List<Peer>();

		static void Main(string[] args)
		{
			ConnectionAccepter connectionAccepter = new ConnectionAccepter();
			rudpClient.Subscribe(connectionAccepter);

			rudpClient.Bind(new System.Net.IPEndPoint(System.Net.IPAddress.Any, socketPort));

			while (true)
			{
				if(Console.ReadKey().Key == ConsoleKey.Escape)
				{
					break;
				}

				System.Threading.Thread.Yield();
			}

			foreach(Peer peer in connectedPeers)
			{
				peer.Disconnect();
			}

			rudpClient.Dispose();
		}

		class ConnectionAccepter : IObserver<Peer>
		{

			public void OnCompleted()
			{
				throw new NotImplementedException();
			}

			public void OnError(Exception error)
			{
				throw new NotImplementedException();
			}

			public void OnNext(Peer value)
			{
				value.Subscribe(new MessageEchoer());
				value.Subscribe(new StatusObserver());
				value.Accept();
				connectedPeers.Add(value);

				Console.WriteLine("New Peer connected: " + value.RemoteEndPoint.ToString());
			}
		}

		class MessageEchoer : IObserver<Message>
		{
			public void OnCompleted()
			{
				throw new NotImplementedException();
			}

			public void OnError(Exception error)
			{
				throw new NotImplementedException();
			}

			public void OnNext(Message value)
			{
				string receivedString = ASCIIEncoding.ASCII.GetString(value.buffer);
				Console.WriteLine(value.peer.RemoteEndPoint.ToString() + ": " + receivedString);

				foreach(Peer peer in connectedPeers)
				{
					peer.Send(value.buffer, MessageResultCallback);
				}
			}

			public void MessageResultCallback(int sequenceNumber, AcknowledgeResult result)
			{
				switch (result)
				{
					case AcknowledgeResult.Acknowledged:
						{
							Console.WriteLine("Message " + sequenceNumber + " Acknowledged");
						}
						break;
					case AcknowledgeResult.Late:
						{
							Console.WriteLine("Message " + sequenceNumber + " is Late");
						}
						break;
					case AcknowledgeResult.Dropped:
						{
							Console.WriteLine("Message " + sequenceNumber + " was Dropped");
						}
						break;
				}
			}
		}

		class StatusObserver : IObserver<StatusMessage>
		{
			public void OnCompleted()
			{
				throw new NotImplementedException();
			}

			public void OnError(Exception error)
			{
				throw new NotImplementedException();
			}

			public void OnNext(StatusMessage value)
			{
				switch (value.state)
				{
					case Peer.State.Disconnected:
						{
							Console.WriteLine("Peer " + value.peer.RemoteEndPoint.ToString() + " has disconnected!");
							connectedPeers.Remove(value.peer);
						}
						break;
					case Peer.State.NonResponsive:
						{
							Console.WriteLine("Peer " + value.peer.RemoteEndPoint.ToString() + " is non responsive, terminating connection!");
							value.peer.Disconnect();
							connectedPeers.Remove(value.peer);
						}
						break;
				}
			}
		}
	}
}
