﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomTests
{
	class Program
	{
		static void Main(string[] args)
		{
			ushort testValue = 0;
			for(int i=0; i<ushort.MaxValue * 3; i++)
			{
				unchecked
				{
					testValue++;
				}

				Console.Write("|{0:D5}|", testValue);
			}

			Console.ReadKey();
		}
	}
}
